# 9-6-hw



# Task
1. Create and set up an infrastructure for deploying a ReactJS application
2. With the code from the repository.
3. Deploy infrastructure using Terraform.
4. In the ansible folder, create a hosts file and add two groups: nginx_servers and reactjs_servers.
5. For the nginx_servers group, add two instances: nginx_1 and nginx_2, define IP addresses for them, the ubuntu user and specify the key with which you can
5. Creating a playbook for deploying a ReactJS application
6. Create a playbook to configure Nginx to proxy requests from the load balancer to the ReactJS application server
7. Set up Nginx servers and deploying ReactJS using Ansible roles
